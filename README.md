This module demonstrates a state caching bug in Drupal.

How to test:
  * Install Drupal 8
  * Install this module
  * Surf to /check_state/<random key> and hit F5 until you see the error message.
  * Or, use apache bench: ab -n 1000 -c 1 http://localhost/check_state/<random_key>
