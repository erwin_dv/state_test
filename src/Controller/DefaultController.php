<?php

namespace Drupal\state_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * This module demonstrates a state caching bug in Drupal.
 *
 * How to test:
 *  Surf to /check_state/<random key> and hit F5 until you see the error message.
 *  Or, use apache bench: ab -n 1000 -c 1 http://localhost/check_state/<random_key>
 *
 *
 */
class DefaultController extends ControllerBase {

  /**
   * @param string $key
   *  The state key to check.
   * @return array $render
   */
  public function checkState($key) {
    // State according to core
    $coreState = \Drupal::state()->get($key, 0);
    
    // State according to database
    $query = \Drupal::database()->query("
      SELECT value
      FROM {key_value}
      WHERE collection = 'state'
      AND name = :key
    ", [':key' => $key]);
    
    $databaseState = unserialize($query->fetchField(0));
    
    
    // Comparison
    drupal_set_message("Checking state key $key");
    
    if ($databaseState !== FALSE && $coreState != $databaseState) {
      drupal_set_message("ERROR! State in database is $databaseState, state according to core is $coreState", 'error');
      drupal_set_message("Rebuild caches or use another key to continue...", 'error');
    } else {
      drupal_set_message("State in database is equal to state in core = $coreState");
      $coreState++;
      \Drupal::state()->set($key, $coreState);
      drupal_set_message("New state is $coreState");
    }
    
    return [ ];
  }
  
}
